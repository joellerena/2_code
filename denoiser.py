from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from datetime import datetime
from six.moves import xrange
import numpy as np
import tensorflow as tf
import initialize
import utilities
import model
from os import listdir
from os.path import isfile, join
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from glob import glob
import time

# denoiser.py: Main file where training and testing are launched


def Train(startIter, params, FLAGS):

  # Generate data
  globalStep = tf.Variable(startIter, trainable=False)
  data = utilities.Inputs(FLAGS.trainFile, 1, FLAGS.trainBatchSize, params['featureListFull'], 
                          params['featureLengthsFull'], params['featureListDiffuse'], 
                          params['featureLengthsDiffuse'], params['featureListSpecular'], 
                          params['featureLengthsSpecular'], FLAGS)

  # Evaluate network
  diffOut, specOut, finalOut = model.EvaluateNetwork(data, params, FLAGS.trainBatchSize, True, FLAGS)

  # Calculate error
  diffLoss = model.Loss(diffOut, data['diffGt'], FLAGS.errorFunc, FLAGS)
  specLoss = model.Loss(specOut, data['specGt'], FLAGS.errorFunc, FLAGS)
  finalLoss = model.Loss(finalOut, data['finalGt'], FLAGS.errorFunc, FLAGS)

  # Backprop
  if FLAGS.trainSeparately:

    # Pretrain diff and spec networks
    trainOp = [model.Train(diffLoss, globalStep, FLAGS), model.Train(specLoss, globalStep, FLAGS)]
  
  else:
    
    # Train full framework
    trainOp = [model.Train(finalLoss, globalStep, FLAGS)]
  
  # Create a saver for checkpoints. max_to_keep=0 means checkpoints aren't deleted after a while.
  saver = tf.train.Saver(tf.global_variables(), max_to_keep=0)

  # Build an initialization operation to run below.
  init = tf.global_variables_initializer()

  # Start running operations on the Graph.
  sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
  coord = tf.train.Coordinator()
  sess.run(init)

  # Load previous variables if continuing
  if FLAGS.continueIters or startIter > 0:
    checkpointPath = params['dirList']['checkpoint'] + ("Checkpoint-%d" % startIter) 
    saver.restore(sess, checkpointPath)

  # Start the queue runners for reading data
  tf.train.start_queue_runners(sess=sess, coord=coord)

  # Loop through training steps.
  endIter = min(startIter + FLAGS.numItersForEval, FLAGS.maxSteps) + 1
  for step in xrange(startIter, endIter):
    if step % 100 == 0:
      if step == startIter:
        print("Running %d iterations before next evaluation\n" % (FLAGS.numItersForEval))
      print("Training iteration %d of %d" % (max(step, 1), FLAGS.maxSteps))
    sess.run(trainOp)

  # Save checkpoints
  checkpointPath = params['dirList']['checkpoint'] + 'Checkpoint' 
  saver.save(sess, checkpointPath, global_step=step)

  # Clean up/reset graph
  coord.request_stop()
  sess.close()
  tf.reset_default_graph()
  
  return step


def Test(inputfile, gtfile, featureListFull, featureLengthsFull, featureListDiff, featureLengthsDiff, 
          featureListSpec, featureLengthsSpec, outDir, step, errors, params, FLAGS):

  startTime = time.time()

  # Generate data
  filename = inputfile.split("/")[-1]
  batchSize = 1 
  data, imgWidth, imgHeight = utilities.ConvertExrToTensor(inputfile, gtfile, batchSize, featureListFull, 
                                                            featureLengthsFull, featureListDiff, 
                                                            featureLengthsDiff, featureListSpec, 
                                                            featureLengthsSpec, FLAGS)



  # Evaluate network
  diffOut, specOut, finalOut = model.EvaluateNetwork(data, params, batchSize, False, FLAGS)

  # Calculate errors
  lossDiff = model.Loss(diffOut, data['diffGt'], FLAGS.errorFunc, FLAGS)
  lossSpec = model.Loss(specOut, data['specGt'], FLAGS.errorFunc, FLAGS)
  loss = model.Loss(finalOut, data['finalGt'], FLAGS.errorFunc, FLAGS)
  ssimLoss = model.Loss(finalOut, data['finalGt'], 'SSIM', FLAGS)
  l1Loss = model.Loss(finalOut, data['finalGt'], 'L1', FLAGS)
  relMseLoss = model.Loss(finalOut, data['finalGt'], 'RELMSE', FLAGS)
  mapeLoss = model.Loss(finalOut, data['finalGt'], 'MAPE', FLAGS)

  evalArgs = [lossDiff, lossSpec, loss, data['diffInput'], diffOut, data['diffGt'], 
              data['specInput'], specOut, data['specGt'], data['finalInput'], 
              finalOut, data['finalGt'], ssimLoss, l1Loss, relMseLoss, mapeLoss, imgWidth, imgHeight]

  # Create a saver for checkpoints. max_to_keep=0 means checkpoints aren't deleted after a while.
  saver = tf.train.Saver(tf.global_variables(), max_to_keep=0)

  # Build an initialization operation to run below.
  init = tf.global_variables_initializer()

  # Start running operations on the Graph.
  sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
  coord = tf.train.Coordinator()
  sess.run(init)

  # Start the queue runners for reading data
  tf.train.start_queue_runners(sess=sess, coord=coord)

  # Load previous variables if continuing
  if step > 0:
    checkpointPath = params['dirList']['checkpoint'] + ("Checkpoint-%d" % step)
    saver.restore(sess, checkpointPath)
 
  preprocessTime = time.time() - startTime
  print("Preprocess time: %f seconds" % (preprocessTime))

  # Evaluate the graph
  evalLoss = utilities.EvaluateAndWriteFullExr(sess, outDir, step, featureListFull, featureListDiff, 
                                                featureLengthsDiff, featureListSpec, featureLengthsSpec, 
                                                evalArgs, filename, FLAGS)
  sess.finalize() #Genera una interrupción
  # Accumulate errors
  errors['final'] += evalLoss['final']
  errors['diff'] += evalLoss['diff']
  errors['spec'] += evalLoss['spec']
  errors['ssim'] += evalLoss['ssim']
  errors['l1'] += evalLoss['l1']
  errors['relmse'] += evalLoss['relmse']
  errors['mape'] += evalLoss['mape']

  # Clean up/reset graph
  coord.request_stop()  
  sess.close()
  tf.reset_default_graph()
  
  totalTime = time.time() - startTime
  print("Total time for image: %f seconds" % (totalTime))

  return errors


def main(argv=None):

  # Initialize experiment parameters
  FLAGS, params = initialize.InitParams()

  # Generate tfrecords for training
  if FLAGS.generateTrainingData:

    utilities.GenerateTrainingData(params['featureListFull'], params['featureLengthsFull'], params['featureListDiffuse'], 
                                    params['featureLengthsDiffuse'], params['featureListSpecular'], params['featureLengthsSpecular'], FLAGS)

  # Otherwise do the actual training
  else:

    # Set up start Iter. If we continueIters, we copy error up until start Iter.
    startIter = 0
    if FLAGS.continueIters:
      startIter = FLAGS.startIter
      if FLAGS.isTraining:
        for folder in params['errorFolders']:
          utilities.UpdateError(params['dirList'][folder], startIter, FLAGS)
    step = startIter

    # Test
    TestAll(step, params, FLAGS)
    step = 32000 #Valor escogido para observaci�n hasta 1000000 en 65000 ocurre el corte 
    # Train/Test loop
    while step < FLAGS.maxSteps and FLAGS.isTraining:

      # Train
      #OOif step == 0:
        #OOprint("\nStarting training...")
      #OOelse:
        #OOprint("\nResuming training...")
      #OOstep = Train(step, params, FLAGS) 
      step = step + 1  #En el archivo de initializer debe estar iniciado con 32000
      # Test
      TestAll(step, params, FLAGS)
      tf.Graph().finalize()
    return


# Run through training evaluation and validation sets
def TestAll(step, params, FLAGS):

  # Train evaluation
  trainErrors = utilities.InitializeErrors()
  if FLAGS.saveTrainingImages and ((step % FLAGS.numItersForEval == 0) or not FLAGS.isTraining):
    print("\nStarting training evaluation...")
    trainErrors = TestAllScenes(FLAGS.trainInDir, FLAGS.trainGtDir, params, trainErrors, params['dirList']['trainImg'], step, FLAGS)
    print("\nDone with training evaluation\n")

  # Validation
  validErrors = utilities.InitializeErrors()
  if step % FLAGS.numItersForEval == 0 or not FLAGS.isTraining:
    print("\nStarting validation...")
    validErrors = TestAllScenes(FLAGS.validInDir, FLAGS.validGtDir, params, validErrors, params['dirList']['validImg'], step, FLAGS)
    print("\nDone with validation\n")

  # Report errors
  trainStr = '%s: Step %d, Train Loss, Diff: %.5f, Spec: %.5f, Final: %.5f, SSIM: %.5f, L1: %.5f, RelMse: %.5f, Mape: %.5f '
  validStr = 'Valid Loss, Diff: %.5f, Spec: %.5f, Final: %.5f, SSIM: %.5f, L1: %.5f, RelMse: %.5f, Mape: %.5f'
  formatStr = trainStr + validStr
  print (formatStr % (datetime.now(), step, trainErrors['diff'], trainErrors['spec'], trainErrors['final'], trainErrors['ssim'], 
    trainErrors['l1'], trainErrors['relmse'], trainErrors['mape'], validErrors['diff'], validErrors['spec'], validErrors['final'], 
    validErrors['ssim'], validErrors['l1'], validErrors['relmse'], validErrors['mape']))

  # Save errors
  err = validErrors['final']
  errorList = [err, trainErrors['final'], validErrors['final']]
  errorListDiff = [validErrors['diff'], trainErrors['diff'], validErrors['diff']]
  errorListSpec = [validErrors['spec'], trainErrors['spec'], validErrors['spec']]
  ssimErrorList = [validErrors['ssim'], trainErrors['ssim'], validErrors['ssim']]
  l1ErrorList = [validErrors['l1'], trainErrors['l1'], validErrors['l1']] 
  relmseErrorList = [validErrors['relmse'], trainErrors['relmse'], validErrors['relmse']] 
  mapeErrorList = [validErrors['mape'], trainErrors['mape'], validErrors['mape']]
  assert(len(errorList) == len(params['errorFolders']))
  errorFilename = "Error.txt"
  if not FLAGS.isTraining:
    errorFilename = errorFilename.replace('.txt', '_%08d_Test.txt' % (step))
  utilities.SaveErrorList(params['dirList'], params['errorFolders'], errorList, errorFilename, step)
  utilities.SaveErrorList(params['dirList'], params['errorFolders'], errorListDiff, "DiffError.txt", step)
  utilities.SaveErrorList(params['dirList'], params['errorFolders'], errorListSpec, "SpecError.txt", step)
  utilities.SaveErrorList(params['dirList'], params['errorFolders'], ssimErrorList, "SSIM.txt", step)
  utilities.SaveErrorList(params['dirList'], params['errorFolders'], l1ErrorList, "L1.txt", step)
  utilities.SaveErrorList(params['dirList'], params['errorFolders'], relmseErrorList, "RelMse.txt", step)
  utilities.SaveErrorList(params['dirList'], params['errorFolders'], mapeErrorList, "Mape.txt", step)


# Test all scenes in a given folder
def TestAllScenes(inputdir, gtdir, params, errors, outdir, step, FLAGS):

  inputfiles = glob(os.path.join(inputdir, '*.exr'))
  inputfiles.sort()
  fileIndex = 1
  for curfile in inputfiles: 
    print("\nWorking on image %d of %d" % (fileIndex, len(inputfiles)))
    fileIndex += 1
    scene = curfile.split("/")[-1]
    gtfile = utilities.FindGt(scene, gtdir, FLAGS)
    errors = Test(curfile, gtfile, params['featureListFull'], params['featureLengthsFull'], params['featureListDiffuse'],
                                     params['featureLengthsDiffuse'], params['featureListSpecular'],
                                     params['featureLengthsSpecular'], outdir, step, errors, params, FLAGS)

  # Take average
  numfiles = len(inputfiles)
  assert(numfiles > 0)
  for key, val in errors.viewitems():
    errors[key] = val / numfiles

  return errors


if __name__ == '__main__':
  tf.app.run()
