from shutil import copyfile
from os.path import isfile, join
from os import listdir
import tensorflow as tf
import os.path

# initialize.py: File that defines all the parameters for the experiment


# Global flags
FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_boolean ('generateTrainingData', 0,
                            'Indicate whether to go into DATA GENERATION (1) or TRAINING (0)')


#### DATA GENERATION ####

tf.app.flags.DEFINE_string  ('inputDir', '../1_Data/Train/Input',
                            'Input directory for training images')
tf.app.flags.DEFINE_string  ('outputDir', '../1_Data/Train/Patches/',
                            'Output directory for training tfrecords')
tf.app.flags.DEFINE_string  ('gtDir', '../1_Data/Train/Gt/',
                            'Directory of reference images')
tf.app.flags.DEFINE_string  ('dataName', 'TrainData',
                            'Name for tfrecord')
tf.app.flags.DEFINE_integer ('numPatchSamples', 400,
                            'Number of patches to sample from each image')
tf.app.flags.DEFINE_boolean ('shuffleFiles', 1,
                            'Shuffle images before looping through and drawing patches from them')
tf.app.flags.DEFINE_boolean ('discardBlackPatches', 1,
                            'Throw away all black patches')
tf.app.flags.DEFINE_boolean ('saveImages', 0,
                            'Save debug images')
tf.app.flags.DEFINE_string  ('debugDir', FLAGS.outputDir + 'DebugImages/',
                            'Directory to save debug images')


#### NETWORK PARAMETERS ####

#00tf.app.flags.DEFINE_string  ('experimentName', '0_TungstenTrainedWeights',
tf.app.flags.DEFINE_string  ('experimentName', '3_TungstenTrainedWeights',
                            'Name of current experiment')  #En nuevo proyecto
#00tf.app.flags.DEFINE_boolean ('isTraining', 0, 
tf.app.flags.DEFINE_boolean ('isTraining', 1, 
                            'Indicate whether this is training or test')  #Entrenamiento 1
tf.app.flags.DEFINE_boolean ('trainSeparately', 1, 
                            'Pretrain diffuse and specular networks')  #Redes separadas
tf.app.flags.DEFINE_boolean ('continueIters', 1,
                            'Load weights and continue training')#Continua entrenamiento desde el valor inicial
#tf.app.flags.DEFINE_integer ('startIter', 360000,
tf.app.flags.DEFINE_integer ('startIter', 32000,
                            'Iter to start from. Valid only if continueIters is set.')  #Valor inicial es 32000 para observar
tf.app.flags.DEFINE_boolean ('isKernelPrediction', 1,
                            'Toggle between kernel-prediction (KPCN) and direct (DPCN). KPCN is default')
tf.app.flags.DEFINE_string  ('errorFunc', 'L1',
                            'Error function to train on. Choose from L1, L2, MAPE, RELMSE, LMLS, or SSIM')
tf.app.flags.DEFINE_float   ('learningRate', 0.0001,
                            'Learning rate for ADAM. Tungsten data default is 0.0001 for pre-training and 0.000001 for fine-tuning')
tf.app.flags.DEFINE_integer ('trainBatchSize', 100, 
                            'Number of patches to process in a training batch.')
tf.app.flags.DEFINE_integer ('maxSteps', 100000000,
                            'A ridiculous number of training iterations. User can (and should) quit before this is reached')
tf.app.flags.DEFINE_integer ('patchSize', 65,
                            'Input patch size.')
tf.app.flags.DEFINE_integer ('reconPatchSize', 21,
                            'Patch size for reconstruction kernel.')
tf.app.flags.DEFINE_integer ('inputChannels', 27,
                            'Number of channels in input patch.')
tf.app.flags.DEFINE_integer ('outputChannels', 3,
                            'Number of channels in output patch.')
tf.app.flags.DEFINE_integer ('seed', 23,
                            'Seed for weight initialization.')

tf.app.flags.DEFINE_string  ('dataDir', '../1_Data/',
                            'Directory where the data lives.')
tf.app.flags.DEFINE_string  ('trainFile',
                            '../1_Data/Train/Patches/TrainData.tfrecord',
                            'Path to the training data.')
tf.app.flags.DEFINE_string  ('validInDir',
                            '../1_Data/Validation/Input/',
                            'Path to the input validation images during evaluation.')
tf.app.flags.DEFINE_string  ('validGtDir',
                            '../1_Data/Validation/Gt/',
                            'Path to the reference validation images during evaluation.')
tf.app.flags.DEFINE_boolean ('saveTrainingImages', 0,
                            'Whether to run the training evaluation')
tf.app.flags.DEFINE_string  ('trainInDir',
                            '../1_Data/TrainEval/Input/',
                            'Path to the input training evaluation images.') 
tf.app.flags.DEFINE_string  ('trainGtDir',
                            '../1_Data/TrainEval/Gt/',
                            'Path to the reference training evaluation images.')
tf.app.flags.DEFINE_string  ('resultsDir', '../3_Results/',
                            'Directory where to write training results.')
tf.app.flags.DEFINE_integer ('numItersForEval', 1000, 
                            'Number of iters before testing the evaluation sets during training.')


# The above flag can't take lists, so we define the remaining parameters we need here
def InitParams():

    # The channels that show up in the tf record input data. Everything except 'Reference', 'diffMap', 'finalInput', and 'finalGt' is
    # automatically considered as part of the input to the networks. 
    featureListFull = ['default', 'Reference', 'gradNormal','gradDepth', 'gradAlbedo', 'gradSpecular', 'gradIrrad', 'diffMap','finalInput', 'finalGt']
    featureListDiffuse = ['default', 'Reference', 'gradNormal','gradDepth', 'gradAlbedo', 'gradIrrad']
    featureListSpecular = ['default', 'Reference', 'gradNormal', 'gradDepth','gradAlbedo','gradSpecular']

    # The lengths of each of the features in featureList. This must be in the same order and have the same length
    # as featureList.
    featureLengthsFull = [6, 6, 7, 3, 7, 7, 7, 3, 3, 3]
    featureLengthsDiffuse = [3, FLAGS.outputChannels, 7, 3, 7, 7]
    featureLengthsSpecular = [3, FLAGS.outputChannels, 7, 3, 7, 7]
    assert (len(featureListFull) == len(featureLengthsFull))
    assert (len(featureListDiffuse) == len(featureLengthsDiffuse))
    assert (len(featureListSpecular) == len(featureLengthsSpecular))

    # The size of each of the layers. There first element is the input size. The last element is output size. In
    # between are the sizes of each hidden layer. Output layer depends on reconstruction type (KPCV or DPCN)
    if FLAGS.isKernelPrediction:
      finalLayerSize = FLAGS.reconPatchSize * FLAGS.reconPatchSize
    else:
      finalLayerSize = FLAGS.outputChannels

    layersDiffuse = [FLAGS.inputChannels, 100, 100, 100, 100, 100, 100, 100, 100, finalLayerSize]
    layersSpecular = [FLAGS.inputChannels, 100, 100, 100, 100, 100, 100, 100, 100, finalLayerSize]

    # This is the convolutional kernel size in each layer. The convolutional kernels are assumed to be symmetrical.
    # If the first element is 5 then that means we have a 5x5 convolutional kernel in the first layer (between input
    # and the next layer). There should be len(layers) - 1 number of kernels
    kernels = [5, 5, 5, 5, 5, 5, 5, 5, 5, None]
    assert (len(layersDiffuse) == len(kernels) and len(layersSpecular) == len(kernels))

    # Dictionary of directoryName: directoryPath for the results
    dirList = InitializeResultsFolder()

    # Set up folders to save errors. Must correspond to folders in dirList (see above)
    errorFolders = ['output', 'trainImg', 'validImg']

    # Set up a dictionary of the parameters
    params = {'featureListFull': featureListFull,
              'featureLengthsFull': featureLengthsFull,
              'featureListDiffuse': featureListDiffuse,
              'featureLengthsDiffuse': featureLengthsDiffuse,
              'featureListSpecular': featureListSpecular,
              'featureLengthsSpecular': featureLengthsSpecular,
              'layersDiffuse': layersDiffuse,
              'layersSpecular': layersSpecular,
              'kernels': kernels,
              'dirList': dirList,
              'errorFolders': errorFolders}

    # Return all parameters
    return FLAGS, params


# Set up the results directory and subfolders
def InitializeResultsFolder():

  dirList = {}

  # Make output folder
  outputDir = FLAGS.resultsDir + FLAGS.experimentName + "/"
  if not os.path.exists(outputDir):
    os.makedirs(outputDir)
  dirList['output'] = outputDir

  # Make checkpoint folder
  checkpointDir = outputDir + "Checkpoints/"
  if not os.path.exists(checkpointDir):
    os.makedirs(checkpointDir)
  dirList['checkpoint'] = checkpointDir

  # Make images folder
  imgDir = outputDir + "Images/"
  if not os.path.exists(imgDir):
    os.makedirs(imgDir)
  dirList['image'] = imgDir

  # Make training images folder
  trainImgDir = imgDir + "Training/"
  if not os.path.exists(trainImgDir):
    os.makedirs(trainImgDir)
  dirList['trainImg'] = trainImgDir

  # Make validation images folder
  validationImgDir = imgDir + "Validation/"
  if not os.path.exists(validationImgDir):
    os.makedirs(validationImgDir)
  dirList['validImg'] = validationImgDir

  # Make a code folder
  codeDir = outputDir + "Code/"
  if not os.path.exists(codeDir):
    os.makedirs(codeDir)

  # Copy code to folder
  onlyFiles = [f for f in listdir('.') if isfile(join('.', f)) and ('.py' in f)]
  for curFile in onlyFiles:
    copyfile(curFile, codeDir + curFile)

  return dirList